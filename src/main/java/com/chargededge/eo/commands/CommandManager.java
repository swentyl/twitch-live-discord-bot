package com.chargededge.eo.commands;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import javax.annotation.Nullable;
import java.util.*;

public class CommandManager {
    private final Map<String, ICommand> commands = new HashMap<>();

    public CommandManager() {
        addCommand(new HelpCommand(this));

    }

    private void addCommand(ICommand cmd) {
        List<String> cmdNames = new ArrayList() {{
            cmd.getAliases().stream().forEach(name -> add(name));
            //addAll(cmd.getAliases());
            add(cmd.getName());
        }};

        boolean nameFound = this.commands.keySet().stream().anyMatch(name -> cmdNames.contains(name));

        if (nameFound) {
            throw new IllegalArgumentException("A command with this name is already present");
        }

        commands.put(cmd.getName(), cmd);
        for (String alias: cmd.getAliases()) {
            commands.put(alias, cmd);
        }
    }

    @Nullable
    public ICommand getCommand(String search) {
        return this.commands.get(search.toLowerCase());
    }

    public List<ICommand> getCommands() {
        return new ArrayList(new HashSet<>(this.commands.values()));
    }

    public void handle(GuildMessageReceivedEvent event, String command, List<String> args) {
        ICommand cmd = this.getCommand(command);

        if (cmd != null) {
            event.getChannel().sendTyping().queue();
            cmd.handle(new CommandContext(event, args));
        }
    }

}
