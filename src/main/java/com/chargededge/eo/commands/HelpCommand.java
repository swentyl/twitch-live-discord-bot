package com.chargededge.eo.commands;


import com.chargededge.eo.Bot;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.List;

public class HelpCommand implements ICommand {

    private final CommandManager manager;

    public HelpCommand(CommandManager manager) {
        this.manager = manager;
    }

    @Override
    public void handle(CommandContext ctx) {
        List<String> args = ctx.getArgs();


        boolean cmdGiven = !(args.isEmpty() || NumberUtils.isParsable(args.get(0)));
        Integer pageNum = (cmdGiven) ? 0 : (args.isEmpty() ? 0 : Integer.parseInt(args.get(0)));
        if (!cmdGiven) {
            StringBuilder builder = new StringBuilder();
            builder.append("List of commands");

            List<ICommand> commands = manager.getCommands();
            int totalCommands = commands.size();
            int maxCommandsPerPage = 10;

            int numPages = (int) Math.ceil((double) totalCommands / maxCommandsPerPage);

            List<ICommand> cmds;
            if (numPages > 1) {
                builder.append(String.format("(%s of %s)\n", pageNum, numPages));

                // Get Starting Page Number
                int startNumber = maxCommandsPerPage * pageNum;

                // Get Remaining Commands
                int cmdsLeft = totalCommands - startNumber;

                // Get Remaining Number of Pages
                int remainingPages = cmdsLeft / maxCommandsPerPage;

                /*
                Get Remaining Pages
                If more than one page, then just add max commands
                If not, find the remaining number of pages
                 */
                int endNumber = (remainingPages > 0) ? (startNumber + maxCommandsPerPage) : (totalCommands % maxCommandsPerPage);

                cmds = commands.subList(startNumber, endNumber);

            }
            else {
                builder.append("\n");
                cmds = commands;
            }


            for (ICommand cmd: cmds) {
                builder.append(String.format("%s%s", Bot.getPrefix(), cmd.getName()));
            }

            //ctx.getEvent().getAuthor().openPrivateChannel().queue();
            ctx.getChannel().sendMessage(builder.toString()).queue();
            return;
        }

        String search = args.get(0);
        ICommand command = manager.getCommand(search);

        if (command == null) {
            ctx.getChannel().sendMessage("Nothing found for " + search).queue();
            return;
        }

        ctx.getChannel().sendMessage(command.getHelp());

    }

    @Override
    public String getName() {
        return "help";
    }


}
