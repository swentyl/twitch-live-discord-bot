package com.chargededge.eo.commands;

import com.chargededge.eo.commands.CommandContext;

import java.util.Arrays;
import java.util.List;

public interface ICommand {

    void handle(CommandContext ctx);

    default String getHelp() {
        return String.format("%s Help", this.getName());
    }

    String getName();

    default List<String> getAliases() {
        return Arrays.asList();
    }
}
