package com.chargededge.eo.listeners;

import com.chargededge.eo.commands.CommandManager;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandListener extends ListenerAdapter {

    private final CommandManager manager = new CommandManager();

    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
        if (event.getAuthor().isBot() || event.isWebhookMessage()) {
            return;
        }

        String message = event.getMessage().getContentRaw();

        if (message.startsWith("!")) {
            List<String> args = new ArrayList(Arrays.asList(message.substring(1).split(" ")));


            String cmd = args.get(0);
            if (cmd.isBlank()) {
                event.getChannel().sendMessage("Invalid Command! Type \"!help\" to get available commands").queue();
                return;
            }

            args.remove(0);

            manager.handle(event, cmd, args);
            //System.out.println(cmd);
            //System.out.println(Arrays.toString(args.toArray()));




        }
    }
}
